'use strict';

// vendor
var gulp = require('gulp'),
    include = require("gulp-include"),
    watch = require('gulp-watch'),
    prefixer = require('gulp-autoprefixer'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    browserSync = require('browser-sync'),
    cssGlobbing = require('gulp-css-globbing'),
    minifyCss = require('gulp-minify-css'),
    uglify = require('gulp-uglify'),
    svgstore = require('gulp-svgstore'),
    svgmin = require('gulp-svgmin'),
    inject = require('gulp-inject'),
    rimraf = require('rimraf'),
    plumber = require('gulp-plumber'),
    jade = require('gulp-jade'),
    jadeGlobbing = require('gulp-jade-globbing'),
    flatten = require('gulp-flatten'),
    rigger = require('gulp-rigger'),
    gutil = require('gulp-util'),
    ftp = require('gulp-ftp'),
    reload = browserSync.reload;

// path
var path = {
    build: {
        html: 'build/',
        js: 'build/js/',
        css: 'build/css/',
        img: 'build/assets/images/',
        illustrations: 'build/assets/illustrations/',
        videos: 'build/assets/videos/',
        fonts: 'build/assets/fonts/'
    },
    src: {
        jade: 'src/7.pages/**/*.jade',
        jsmain: 'src/1.foundation/main.js',
        jsasync: 'src/1.foundation/async.js',
        style: 'src/1.foundation/main.scss',
        img: ['src/6.assets/images/**/*.{jpg,jpeg,png,gif,ico}', 'src/2.vendors/**/*.{jpg,jpeg,png,gif,ico}'],
        icons: 'src/6.assets/icons/**/*.svg',
        illustrations: ['src/6.assets/illustrations/**/*.svg', 'src/2.vendors/**/*.svg'],
        videos: 'src/6.assets/videos/**/*.{mp4,webm}',
        fonts: 'src/6.assets/fonts/**/*.{eot,ttf,woff,otf}'
    },
    watch: {
        jade: 'src/**/*.jade',
        js: 'src/**/*.js',
        style: 'src/**/*.scss',
        img: 'src/6.assets/**/*.{jpg,jpeg,png,gif}',
        icons: 'src/6.assets/icons/**/*.svg',
        illustrations: 'src/6.assets/illustrations/**/*.svg',
        videos: 'src/6.assets/**/*.{mp4,webm}',
        fonts: 'src/6.assets/fonts/**/*.*'
    },
    clean: 'build/'
};

// local server
var config = {
    server: {
        baseDir: "./build"
    },
    tunnel: false,
    notify: false,
    ui: false,
    host: 'localhost',
    port: 9000,
    logPrefix: ""
};

gulp.task('webserver', function () {
    browserSync(config);
});

// deploy
gulp.task('deploy', function () {
    return gulp.src('build/**/*')
        .pipe(ftp({
            host: 'djove.ru',
            remotePath: 'public_html/',
            user: 'jzpglidj',
            pass: 'Shugaev1989'
        }))
        .pipe(gutil.noop());
});

// delete build
gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});

// build html
gulp.task('jade:build', function () {
    var svgicons = gulp
        .src(path.src.icons)
            .pipe(svgmin({}))
            .pipe(svgstore({inlineSvg: true}));

    function fileContents(filePath, file) {
        return file.contents.toString();
    }

    return gulp
        .src(path.src.jade)
            .pipe(plumber({
                errorHandler: function (error) {
                    console.log(error.message);
                    this.emit('end');
                }
            }))
            .pipe(jadeGlobbing({}))
            .pipe(jade({}))
            .pipe(flatten())
            .pipe(inject(svgicons, {transform: fileContents}))
            .pipe(gulp.dest(path.build.html))
            .pipe(reload({stream: true}));
});

// build css
gulp.task('style:build', function () {
    gulp.src(path.src.style)
        .pipe(plumber({
            errorHandler: function (error) {
                console.log(error.message);
                this.emit('end');
            }
        }))
        .pipe(cssGlobbing({
            extensions: ['.css', '.scss'],
            autoReplaceBlock: {
                onOff: false
            }
        }))
        .pipe(sass())
        .pipe(prefixer())
        .pipe(minifyCss())
        .pipe(flatten())
        .pipe(gulp.dest(path.build.css))
        .pipe(reload({stream: true}));
});

// build js
gulp.task('js:build', function () {
    gulp.src(path.src.jsasync)
        .pipe(plumber({
            errorHandler: function (error) {
                console.log(error.message);
                this.emit('end');
            }
        }))
        .pipe(include())
        .pipe(uglify())
        .pipe(flatten())
        .pipe(gulp.dest(path.build.js));

    gulp.src(path.src.jsmain)
        .pipe(plumber({
            errorHandler: function (error) {
                console.log(error.message);
                this.emit('end');
            }
        }))
        .pipe(include())
        .pipe(uglify())
        .pipe(flatten())
        .pipe(gulp.dest(path.build.js))
        .pipe(reload({stream: true}));
});

// build images
gulp.task('image:build', function () {
    gulp.src(path.src.img)
        .pipe(gulp.dest(path.build.img));
});

// build illustrations
gulp.task('illustrations:build', function () {
    gulp.src(path.src.illustrations)
        .pipe(svgmin({}))
        .pipe(flatten())
        .pipe(gulp.dest(path.build.illustrations))
        .pipe(reload({stream: true}));
});

// build videos
gulp.task('videos:build', function () {
    gulp.src(path.src.videos)
        .pipe(flatten())
        .pipe(gulp.dest(path.build.videos));
});

// build fonts
gulp.task('fonts:build', function () {
    gulp.src(path.src.fonts)
        .pipe(flatten())
        .pipe(gulp.dest(path.build.fonts));
});

// build robots
gulp.task('robots:build', function () {
    gulp.src('robots.txt')
        .pipe(gulp.dest('build/'))
});

// build all
gulp.task('build', [
    'jade:build',
    'style:build',
    'js:build',
    'fonts:build',
    'image:build',
    'illustrations:build',
    'videos:build',
    'robots:build'
]);

// watch
gulp.task('watch', function () {
    watch([path.watch.jade], function (event, cb) {
        gulp.start('jade:build');
    });
    watch([path.watch.icons], function (event, cb) {
        gulp.start('jade:build');
    });
    watch([path.watch.style], function (event, cb) {
        gulp.start('style:build');
    });
    watch([path.watch.js], function (event, cb) {
        gulp.start('js:build');
    });
    watch([path.watch.img], function (event, cb) {
        gulp.start('image:build');
    });
    watch([path.watch.illustrations], function (event, cb) {
        gulp.start('illustrations:build');
    });
    watch([path.watch.videos], function (event, cb) {
        gulp.start('videos:build');
    });
    watch([path.watch.fonts], function (event, cb) {
        gulp.start('fonts:build');
    });
});

// start
gulp.task('default', ['clean'], (function () {
    gulp.start('build', 'webserver', 'watch')
}));