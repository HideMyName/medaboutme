### Установка ###

1) Убедиться что установлен [nodejs](https://nodejs.org/en/)


2) Загрузить файлы проекта любым удобным способом


3) Установить Gulp и плагины, выполнив в корневой директории проекта: 
```
#!console
npm install
```

4) Запуск сервера: 
```
#!console
gulp
```

5) Деплой на djove.ru:  
```
#!console
gulp deploy
```

6) Прочие настройки сборщика в gulpfile.js

7) [Видео](https://www.dropbox.com/s/mdg4w9rvumhi84v/%D0%98%D0%BD%D1%81%D1%82%D1%80%D1%83%D0%BA%D1%86%D0%B8%D1%8F.avi?dl=0)